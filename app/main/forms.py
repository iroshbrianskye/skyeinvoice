from flask import request
from flask_wtf import FlaskForm
from wtforms import Form as NoCsrfForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, IntegerField, DecimalField, DateField, SelectField, FieldList, TextAreaField, FormField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo
from flask_babel import _, lazy_gettext as _l
from app.models import Client, Product, User
from flask_login import current_user
from flask_wtf.file import FileField, FileRequired, FileAllowed
from flask_uploads import UploadSet, configure_uploads, IMAGES
photos = UploadSet('photos', IMAGES)

class ClientForm(FlaskForm):
    client_name  = StringField('client name', validators=[DataRequired()])
    client_type  = StringField('client type', validators=[DataRequired()])
    phone_number = IntegerField('phone number', validators=[DataRequired()])
    email        = StringField('email', validators=[DataRequired(), Email()])
    location     = StringField('location', validators=[DataRequired()])
    submit       = SubmitField('save')


class ProductForm(NoCsrfForm):
    product           = StringField('product', validators=[DataRequired()])
    price             = IntegerField('price', validators=[DataRequired()])
    quantity          = IntegerField('quantity', validators=[DataRequired()])
    size              = IntegerField('size', validators=[DataRequired()])
    total             = IntegerField('total', validators=[DataRequired()])

class QuotationForm(FlaskForm):
    client_id         = SelectField('client_id',coerce=int, id='select_client')
    quotation_name    = StringField('quotation_name', validators=[DataRequired()])
    quotation_date    = DateField('quotation_date',format='%d/%m/%Y')
    products          = FieldList(FormField(ProductForm), min_entries=1)
    amount            = IntegerField('amount', validators=[DataRequired()])
    description       = TextAreaField('description', validators=[DataRequired()])
    submit            = SubmitField('save')

class EditQuotationForm(FlaskForm):
    client_id         = SelectField('client_id',coerce=int, id='select_client')
    quotation_name    = StringField('quotation_name', validators=[DataRequired()])
    quotation_date    = DateField('quotation_date',format='%d/%m/%Y')
    products          = FieldList(FormField(ProductForm,default=lambda: Product()), min_entries=1)
    amount            = IntegerField('amount', validators=[DataRequired()])
    description       = TextAreaField('description', validators=[DataRequired()])
    submit            = SubmitField('save')

class BillForm(FlaskForm):
    supplier_id       = SelectField('supplier_id',coerce=int, id='select_supplier')
    bill_name         = StringField('bill_name', validators=[DataRequired()])
    date_due          = DateField('date_due',format='%d/%m/%Y')
    amount            = IntegerField('amount', validators=[DataRequired()])
    description       = TextAreaField('description', validators=[DataRequired()])
    submit            = SubmitField('save')


class PaymentForm(FlaskForm):
    invoice_id        = SelectField('invoice_id',coerce=int, id='select_invoice')
    payment_mode      = SelectField('payment_mode',choices=[('cash', 'cash'), ('cheque', 'cheque'), ('online-payment', 'online payment')])
    payment_type      = StringField('payment_type', validators=[DataRequired()])
    amount            = IntegerField('amount', validators=[DataRequired()])
    note              = TextAreaField('note', validators=[DataRequired()])
    submit            = SubmitField('save')

class SupplierForm(FlaskForm):
    supplier_name  = StringField('supplier name name', validators=[DataRequired()])
    supplier_type  = StringField('supplier type', validators=[DataRequired()])
    phone_number   = IntegerField('phone number', validators=[DataRequired()])
    email          = StringField('email', validators=[DataRequired(), Email()])
    location       = StringField('location', validators=[DataRequired()])
    bank_name       = StringField('bank_name', validators=[DataRequired()])
    bank_country    = StringField('bank_country', validators=[DataRequired()])
    bank_address    = StringField('bank_address', validators=[DataRequired()])
    account_number  = IntegerField('account_number', validators=[DataRequired()])
    submit         = SubmitField('save')

class EditPasswordForm(FlaskForm):
    oldpassword = PasswordField('Password', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(),
                                           EqualTo('password')])
    submit = SubmitField('Save')

    def validate_oldpassword(self, oldpassword):
        user = User.query.filter_by(id=current_user.id).first()
        if user is None or not user.check_password(oldpassword.data):
            raise ValidationError('Please enter correct Password.')

class RegistrationForm(FlaskForm):
    username = StringField(_l('Username'), validators=[DataRequired()])
    email = StringField(_l('Email'), validators=[DataRequired(), Email()])
    phone_number = StringField(_l('phone_number'), validators=[DataRequired()])
    role = SelectField('role',coerce=int)
    password = PasswordField(_l('Password'), validators=[DataRequired()])
    password2 = PasswordField(
        _l('Repeat Password'), validators=[DataRequired(),
                                           EqualTo('password')])
    submit = SubmitField(_l('Save'))

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError(_('Please use a different email address.'))

class EditRegistrationForm(FlaskForm):
    username = StringField(_l('Username'), validators=[DataRequired()])
    phone_number = StringField(_l('phone_number'), validators=[DataRequired()])
    role = SelectField('role',coerce=int)
    submit = SubmitField(_l('Save'))

class CompanyForm(FlaskForm):
    company_name    = StringField('company_name', validators=[DataRequired()])
    contact_person  = StringField('contact_person', validators=[DataRequired()])
    address         = StringField('address', validators=[DataRequired()])
    country         = StringField('country', validators=[DataRequired()])
    city            = StringField('city', validators=[DataRequired()])
    state           = StringField('state', validators=[DataRequired()])
    postal_code     = StringField('postal_code', validators=[DataRequired()])
    email           = StringField('email', validators=[DataRequired(), Email()])
    phone_number    = IntegerField('phone_number', validators=[DataRequired()])
    bank_name       = StringField('bank_name', validators=[DataRequired()])
    bank_country    = StringField('bank_country', validators=[DataRequired()])
    bank_address    = StringField('bank_address', validators=[DataRequired()])
    account_number  = IntegerField('account_number', validators=[DataRequired()])
    submit          = SubmitField('Save & update')

class ThemeForm(FlaskForm):
    company_name = StringField('company_name', validators=[DataRequired()])
    logo = FileField(validators=[FileAllowed(photos, u'Image only!'), FileRequired(u'File was empty!')])
    favicon = FileField(validators=[FileAllowed(photos, u'Image only!'), FileRequired(u'File was empty!')])
    submit = SubmitField(u'Save')
