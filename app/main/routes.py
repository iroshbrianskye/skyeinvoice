from datetime import datetime
from flask import render_template, flash, redirect, url_for, request, g, \
    jsonify, current_app
from flask_login import current_user, login_required
from flask_babel import _, get_locale
from flask_uploads import UploadSet, configure_uploads, IMAGES
from guess_language import guess_language
from app import db
from app.models import User, Client, Quotation, Product, Invoice, Bill, Supplier, Payment, Role, Permission, Company, \
    Theme
from app.main import bp
from app.main.forms import ClientForm, QuotationForm, BillForm, SupplierForm, PaymentForm, EditQuotationForm, \
    EditPasswordForm, RegistrationForm, EditRegistrationForm, CompanyForm, ThemeForm


@bp.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
    g.locale = str(get_locale())
    g.company = Company.query.first()
    g.images = Theme.query.first()


# index
@bp.route('/', methods=['GET', 'POST'])
@bp.route('/index', methods=['GET', 'POST'])
@bp.route('/dashboard', methods=['GET', 'POST'])
@login_required
def index():
    quotation_count = Quotation.query.count()
    invoice_count = Invoice.query.count()
    bill_count = Bill.query.count()
    payment_count = Payment.query.count()
    user_count = User.query.count()
    count_all = {'quotation': quotation_count, 'invoice': invoice_count, 'bill': bill_count, 'payment': payment_count}
    quotation = Quotation.query.limit(5)
    bill = Bill.query.limit(5)
    invoice = Invoice.query.limit(5)
    payment = Payment.query.limit(5)
    tables = {'quotation': quotation, 'invoice': invoice, 'bill': bill, 'payment': payment}
    return render_template('index.html', count_all=count_all, tables=tables)


# admin functions
@bp.route('/users', methods=['GET', 'POST'])
@login_required
def users():
    if current_user.role.access_user != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = RegistrationForm()
    form.role.choices = [(row.id, row.role) for row in Role.query.order_by(Role.created_at.desc()).all()]
    if form.validate_on_submit():
        role = Role.query.filter_by(id=form.role.data).first_or_404()
        user = User(username=form.username.data, phone_number=form.phone_number.data, role=role, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('User added successfully!', 'success')
        return redirect(url_for('main.users'))
    users = User.query.order_by(User.last_seen.desc()).all()
    return render_template('admin/users.html', form=form, users=users)


# edit user
@bp.route('/edit_user/<id>', methods=['GET', 'POST'])
@login_required
def edit_user(id):
    if current_user.role.permissions.write_user != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    user = User.query.filter_by(id=id).first_or_404()
    form = EditRegistrationForm(obj=user)
    form.role.choices = [(row.id, row.role) for row in Role.query.order_by(Role.created_at.desc()).all()]
    if form.validate_on_submit():
        role = Role.query.filter_by(id=form.role.data).first_or_404()
        user.username = form.username.data
        user.role = role
        user.phone_number = form.phone_number.data
        db.session.commit()
        flash('User edited successfully', 'success')
        return redirect(url_for('main.users'))
    return render_template('admin/edit_user.html', form=form, title='Edit User')


# add role
@bp.route('/add_role', methods=['GET', 'POST'])
@login_required
def add_role():
    if request.method == 'POST':
        role = Role(role=request.form['role'])
        permission = Permission(role=role)
        db.session.add(role)
        db.session.commit()
    return jsonify({'status': 1})


# view user
@bp.route('/view_user/<id>')
@login_required
def view_user(id):
    if current_user.role.permissions.read_user != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    user = User.query.filter_by(id=id).first_or_404()
    return render_template('admin/view_user.html', title='view user', user=user)


# roles
@bp.route('/role_permissions', methods=['GET', 'POST'])
@login_required
def role_permissions():
    if current_user.role.permissions.read_user != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    roles = Role.query.order_by(Role.created_at.desc()).all()
    return render_template('admin/role_permissions.html', roles=roles, title='Roles & Permission')


# return permissions values
@bp.route('/role_permissions_values', methods=['GET', 'POST'])
@login_required
def role_permissions_values():
    role = Role.query.order_by(Role.created_at.desc()).first()
    if role is None:
        return jsonify({'status': 0})
    if request.method == 'POST':
        role = Role.query.filter_by(id=request.form['id']).first_or_404()
    response = jsonify({'status': 1, 'data': role.to_dict_role()})
    return response


# edit role permissions json
@bp.route('/edit_role_permissions_js', methods=['GET', 'POST'])
@login_required
def role_permissions_js():
    role = Role.query.filter_by(id=request.form['id']).first_or_404()
    data = request.form
    role.from_dict(data)
    role.permissions.from_dict_permissions(data)
    db.session.commit()
    return jsonify({'status': 1})


# company settings
@bp.route('/company_settings', methods=['GET', 'POST'])
@login_required
def company_settings():
    if current_user.role.permissions.read_user != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    company = Company.query.first()
    if company is None:
        form = CompanyForm()
        if form.validate_on_submit():
            company = Company(company_name=form.company_name.data, contact_person=form.contact_person.data,
                              phone_number=form.phone_number.data, email=form.email.data, address=form.address.data,
                              country=form.country.data, city=form.city.data, state=form.state.data,
                              postal_code=form.postal_code.data, bank_name=form.bank_name.data,
                              bank_country=form.bank_country.data, bank_address=form.bank_address.data,
                              account_number=form.account_number.data)
            db.session.add(company)
            db.session.commit()
            flash('company settings updated successfully', 'success')
            return redirect(url_for('main.company_settings'))
    else:
        form = CompanyForm(obj=company)
        if form.validate_on_submit():
            form.populate_obj(company)
            db.session.commit()
            flash('company settings updated successfully', 'success')
            return redirect(url_for('main.company_settings'))
    return render_template('admin/company_settings.html', form=form, title='Company Settings')


# theme_settings
@bp.route('/theme_settings', methods=['GET', 'POST'])
@login_required
def theme_settings():
    if current_user.role.permissions.read_user != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    theme = Theme.query.first()
    if theme is None:
        form = ThemeForm()
        if form.validate_on_submit():
            photos = UploadSet('photos', IMAGES)
            logo = photos.save(form.logo.data)
            favicon = photos.save(form.favicon.data)
            theme = Theme(company_name=form.company_name.data, logo=logo, favicon=favicon)
            db.session.add(theme)
            db.session.commit()
            flash('theme settings updated successfully', 'success')
            return redirect(url_for('main.theme_settings'))
    else:
        form = ThemeForm(obj=theme)
        if form.validate_on_submit():
            photos = UploadSet('photos', IMAGES)
            logo = photos.save(form.logo.data)
            favicon = photos.save(form.favicon.data)
            theme.company_name = form.company_name.data
            theme.logo = logo
            theme.favicon = favicon
            db.session.commit()
            flash('theme settings updated successfully', 'success')
            return redirect(url_for('main.theme_settings'))
    return render_template('admin/theme_settings.html', form=form, title='Theme Settings')


# settings
@bp.route('/settings', methods=['GET', 'POST'])
@login_required
def settings():
    form = EditPasswordForm()
    if form.validate_on_submit():
        current_user.set_password(form.password.data)
        db.session.commit()
        flash('Password changed successfully', 'success')
        return redirect(url_for('main.settings'))
    return render_template('settings.html', form=form, title='Edit Password')


# end of admin

# invoice
@bp.route('/invoices')
@login_required
def invoices():
    if current_user.role.access_invoice != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    invoices = Invoice.query.order_by(Invoice.created_at.desc()).all()
    return render_template('invoices.html', title='Invoice', invoices=invoices)


# quotations
@bp.route('/quotations')
@login_required
def quotations():
    if current_user.role.access_quotation != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    quotations = Quotation.query.order_by(Quotation.created_at.desc()).all()
    return render_template('quotations.html', title='Quotations', quotations=quotations)


# bills
@bp.route('/bills')
@login_required
def bills():
    if current_user.role.access_bill != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    bills = Bill.query.order_by(Bill.created_at.desc()).all()
    return render_template('bills.html', title='Bill', bills=bills)


# payments
@bp.route('/payments')
@login_required
def payments():
    if current_user.role.access_payment != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    payments = Payment.query.order_by(Payment.created_at.desc()).all()
    return render_template('payments.html', title='Payment', payments=payments)


# Clients
@bp.route('/clients', methods=['GET', 'POST'])
@login_required
def clients():
    if current_user.role.access_client != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = ClientForm()
    if form.validate_on_submit():
        client = Client(client_name=form.client_name.data, client_type=form.client_type.data,
                        phone_number=form.phone_number.data, email=form.email.data, location=form.location.data)
        db.session.add(client)
        db.session.commit()
        flash('Client added successfully', 'success')
        return redirect(url_for('main.clients'))
    clients = Client.query.order_by(Client.created_at.desc()).all()
    return render_template('clients.html', title='clients', form=form, clients=clients)


# suppliers
@bp.route('/suppliers', methods=['GET', 'POST'])
@login_required
def suppliers():
    if current_user.role.access_supplier != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = SupplierForm()
    if form.validate_on_submit():
        supplier = Supplier(supplier_name=form.supplier_name.data, supplier_type=form.supplier_type.data,
                            phone_number=form.phone_number.data, email=form.email.data,
                            location=form.location.data, bank_name=form.bank_name.data,
                            bank_country=form.bank_country.data,
                            bank_address=form.bank_address.data, account_number=form.account_number.data)
        db.session.add(supplier)
        db.session.commit()
        flash('Supplier added successfully', 'success')
        return redirect(url_for('main.suppliers'))
    suppliers = Supplier.query.order_by(Supplier.created_at.desc()).all()
    return render_template('suppliers.html', title='suppliers', form=form, suppliers=suppliers)


# create quotation
@bp.route('/create_quotation', methods=['GET', 'POST'])
@login_required
def create_quotation():
    if current_user.role.permissions.create_quotation != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = QuotationForm()
    form.client_id.choices = [(row.id, row.client_name) for row in
                              Client.query.order_by(Client.created_at.desc()).all()]
    if form.validate_on_submit():

        quotation_number = 'QUO-001'
        quotation = Quotation.query.order_by(Quotation.created_at.desc()).first()
        if quotation is not None:
            quot_no = quotation.quotation_no.strip('QUO-')
            no = 1
            quotation_no = int(quot_no) + int(no)
            quotation_number = "QUO-00" + str(quotation_no)
        client = Client.query.filter_by(id=form.client_id.data).first_or_404()
        quotation = Quotation(quotation_name=form.quotation_name.data, quotation_no=quotation_number,
                              quotation_date=form.quotation_date.data,
                              description=form.description.data, amount=form.amount.data, client=client)
        db.session.add(quotation)
        db.session.commit()
        quotation_id = Quotation.query.order_by(Quotation.created_at.desc()).first()

        for item in form.products.data:
            products = Product(product=item['product'], price=item['price'], quantity=item['quantity'],
                               size=item['size'], total=item['total'], quotation=quotation_id)
            db.session.add(products)
            db.session.commit()

        flash('Quotation added successfully', 'success')
        return redirect(url_for('main.quotations'))

    return render_template('create_quotation.html', title="create Quotation", form=form)


# create quotation
@bp.route('/create_invoice', methods=['POST'])
@login_required
def create_invoice():
    if current_user.role.permissions.create_invoice != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    invoice_number = 'INV-001'
    invoice = Invoice.query.order_by(Invoice.created_at.desc()).first()
    if invoice is not None:
        inv_no = invoice.invoice_no.strip('INV-')
        no = 1
        invoice_no = int(inv_no) + int(no)
        invoice_number = "INV-00" + str(invoice_no)

    id = request.form['id']
    date_due = request.form['due_date']
    date = datetime.strptime(date_due, '%Y-%m-%d')
    due_date = datetime.combine(date, datetime.min.time())
    quotation = Quotation.query.filter_by(id=id).first_or_404()
    save_invoice = Invoice(invoice_no=invoice_number, date_due=due_date, quotation=quotation)
    db.session.add(save_invoice)
    db.session.commit()
    invoice = Invoice.query.order_by(Invoice.created_at.desc()).first_or_404()

    return jsonify({'status': 1, 'id': invoice.id})


# create bill
@bp.route('/create_bill', methods=['GET', 'POST'])
@login_required
def create_bill():
    if current_user.role.permissions.create_bill != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = BillForm()
    form.supplier_id.choices = [(row.id, row.supplier_name) for row in
                                Supplier.query.order_by(Supplier.created_at.desc()).all()]

    if form.validate_on_submit():

        bill_number = 'BIL-001'
        bill = Bill.query.order_by(Bill.created_at.desc()).first()
        if bill is not None:
            bi_no = bill.bill_no.strip('BIL-')
            no = 1
            bill_no = int(bi_no) + int(no)
            bill_number = "BIL-00" + str(bill_no)
        supplier = Supplier.query.filter_by(id=form.supplier_id.data).first_or_404()
        bill = Bill(bill_name=form.bill_name.data, bill_no=bill_number, date_due=form.date_due.data,
                    description=form.description.data, amount=form.amount.data, supplier=supplier)
        db.session.add(bill)
        db.session.commit()
        flash('Bill added successfully', 'success')
        return redirect(url_for('main.bills'))
    return render_template('create_bill.html', title="create Bill", form=form)


# create payment
@bp.route('/create_payment', methods=['GET', 'POST'])
@login_required
def create_payment():
    if current_user.role.permissions.create_payment != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    form = PaymentForm()
    form.invoice_id.choices = [(row.id, row.quotation.quotation_name) for row in
                               Invoice.query.order_by(Invoice.created_at.desc()).all()]

    if form.validate_on_submit():
        invoice = Invoice.query.filter_by(id=form.invoice_id.data).first_or_404()
        payment = Payment(payment_mode=form.payment_mode.data, payment_type=form.payment_type.data,
                          note=form.note.data, amount=form.amount.data, invoice=invoice)
        db.session.add(payment)
        db.session.commit()
        flash('Payment added successfully', 'success')
        return redirect(url_for('main.payments'))
    return render_template('create_payment.html', title="create Payment", form=form)


# edit bill
@bp.route('/edit_bill/<id>', methods=['GET', 'POST'])
@login_required
def edit_bill(id):
    if current_user.role.permissions.write_bill != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    bill = Bill.query.filter_by(id=id).first_or_404()
    form = BillForm(obj=bill)
    form.supplier_id.choices = [(row.id, row.supplier_name) for row in
                                Supplier.query.order_by(Supplier.created_at.desc()).all()]

    if form.validate_on_submit():
        form.populate_obj(bill)
        db.session.commit()
        flash('Bill edited successfully', 'success')
        return redirect(url_for('main.bills'))

    return render_template('edit_bill.html', form=form, title='Edit Bill')


# edit quotation
@bp.route('/edit_quotation/<id>', methods=['GET', 'POST'])
@login_required
def edit_quotation(id):
    if current_user.role.permissions.write_quotation != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    quotation = Quotation.query.filter_by(id=id).first_or_404()
    form = EditQuotationForm(obj=quotation)
    form.client_id.choices = [(row.id, row.client_name) for row in
                              Client.query.order_by(Client.created_at.desc()).all()]

    if form.validate_on_submit():
        form.populate_obj(quotation)
        db.session.commit()
        flash('Quotation edited successfully', 'success')
        return redirect(url_for('main.quotations'))
    return render_template('edit_quotation.html', form=form)


# edit payment
@bp.route('/edit_payment/<id>', methods=['GET', 'POST'])
@login_required
def edit_payment(id):
    if current_user.role.permissions.write_payment != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    payment = Payment.query.filter_by(id=id).first_or_404()
    form = PaymentForm(obj=payment)
    form.invoice_id.choices = [(row.id, row.quotation.quotation_name) for row in
                               Invoice.query.order_by(Invoice.created_at.desc()).all()]

    if form.validate_on_submit():
        form.populate_obj(payment)
        db.session.commit()
        flash('Payment edited successfully', 'success')
        return redirect(url_for('main.payments'))
    return render_template('edit_payment.html', form=form, title='Edit Payment')


# edit client
@bp.route('/edit_client/<id>', methods=['GET', 'POST'])
@login_required
def edit_client(id):
    client = Client.query.filter_by(id=id).first_or_404()
    form = ClientForm(obj=client)
    if form.validate_on_submit():
        form.populate_obj(client)
        db.session.commit()
        flash('Client edited successfully', 'success')
        return redirect(url_for('main.clients'))
    elif request.method == 'GET':
        data = {'client_type': client.client_type, 'client_name': client.client_name,
                'phone_number': client.phone_number, 'email': client.email, 'location': client.location}
        return jsonify({'status': 1, 'data': data})


# edit supplier
@bp.route('/edit_supplier/<id>', methods=['GET', 'POST'])
@login_required
def edit_supplier(id):
    supplier = Supplier.query.filter_by(id=id).first_or_404()
    form = SupplierForm(obj=supplier)
    if form.validate_on_submit():
        form.populate_obj(supplier)
        db.session.commit()
        flash('Supplier edited successfully', 'success')
        return redirect(url_for('main.suppliers'))
    elif request.method == 'GET':
        data = {'supplier_type': supplier.supplier_type, 'supplier_name': supplier.supplier_name,
                'phone_number': supplier.phone_number, 'email': supplier.email,
                'location': supplier.location, 'bank_name': supplier.bank_name, 'bank_country': supplier.bank_country,
                'bank_address': supplier.bank_address, 'account_number': supplier.account_number}
        return jsonify({'status': 1, 'data': data})


# edit role
@bp.route('/edit_role/<id>', methods=['GET', 'POST'])
@login_required
def edit_role(id):
    role = Role.query.filter_by(id=id).first_or_404()
    if request.method == 'POST':
        role.role = request.form['role']
        db.session.commit()
        return jsonify({'status': 1})
    elif request.method == 'GET':
        data = {'role': role.role, 'id': role.id}
        return jsonify({'status': 1, 'data': data})


# view bill
@bp.route('/view_bill/<id>')
@login_required
def view_bill(id):
    if current_user.role.permissions.read_bill != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    bill = Bill.query.filter_by(id=id).first_or_404()
    return render_template('view_bill.html', title='view bill', bill=bill)


# view invoice
@bp.route('/view_invoice/<id>')
@login_required
def view_invoice(id):
    if current_user.role.permissions.read_invoice != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    invoice = Invoice.query.filter_by(id=id).first_or_404()
    return render_template('view_invoice.html', title='view invoice', invoice=invoice)


# view quotation
@bp.route('/view_quotation/<id>')
@login_required
def view_quotation(id):
    if current_user.role.permissions.read_quotation != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    quotation = Quotation.query.filter_by(id=id).first_or_404()
    return render_template('view_quotation.html', title='view quotation', quotation=quotation)


# view payment
@bp.route('/view_payment/<id>')
@login_required
def view_payment(id):
    if current_user.role.permissions.read_payment != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    payment = Payment.query.filter_by(id=id).first_or_404()
    return render_template('view_payment.html', title='view payment', payment=payment)


# view clients
@bp.route('/view_client/<id>')
@login_required
def view_client(id):
    if current_user.role.permissions.read_client != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    client = Client.query.filter_by(id=id).first_or_404()
    return render_template('view_client.html', title='view client', client=client)


# view suppliers
@bp.route('/view_supplier/<id>')
@login_required
def view_supplier(id):
    if current_user.role.permissions.read_supplier != True:
        flash("You don't have access to this module!", 'danger')
        return redirect(url_for('main.index'))
    supplier = Supplier.query.filter_by(id=id).first_or_404()
    return render_template('view_supplier.html', title='view supplier', supplier=supplier)


# approve
@bp.route('/approve/<tablename>/<id>')
@login_required
def approve(tablename, id):
    if tablename == 'quotations':
        table = Quotation.query.filter_by(id=id).first_or_404()
    elif tablename == 'invoices':
        table = Invoice.query.filter_by(id=id).first_or_404()
    elif tablename == 'bills':
        table = Bill.query.filter_by(id=id).first_or_404()
    elif tablename == 'payments':
        table = Payment.query.filter_by(id=id).first_or_404()
    else:
        flash('error encountered', 'danger')
        return redirect(url_for('main.index'))
    table.status = 'approved'
    db.session.commit()
    flash('{} approved successfully'.format(tablename), 'success')
    return redirect(url_for('main.{}'.format(tablename)))


# reject
@bp.route('/reject/<tablename>/<id>')
@login_required
def reject(tablename, id):
    if tablename == 'quotations':
        table = Quotation.query.filter_by(id=id).first_or_404()
    elif tablename == 'invoices':
        table = Invoice.query.filter_by(id=id).first_or_404()
    elif tablename == 'bills':
        table = Bill.query.filter_by(id=id).first_or_404()
    elif tablename == 'payments':
        table = Payment.query.filter_by(id=id).first_or_404()
    else:
        flash('error encountered', 'danger')
        return redirect(url_for('main.index'))
    table.status = 'rejected'
    db.session.commit()
    flash('{} rejected successfully'.format(tablename), 'success')
    return redirect(url_for('main.{}'.format(tablename)))


# delete
@bp.route('/delete', methods=['GET', 'POST'])
@login_required
def delete():
    id = request.form['id']
    tablename = request.form['table']

    if tablename == 'Client':
        table = Client.query.filter_by(id=id).first_or_404()
    elif tablename == 'Quotation':
        table = Quotation.query.filter_by(id=id).first_or_404()
    elif tablename == 'Invoice':
        table = Invoice.query.filter_by(id=id).first_or_404()
    elif tablename == 'Bill':
        table = Bill.query.filter_by(id=id).first_or_404()
    elif tablename == 'Payment':
        table = Payment.query.filter_by(id=id).first_or_404()
    elif tablename == 'Supplier':
        table = Supplier.query.filter_by(id=id).first_or_404()
    elif tablename == 'User':
        table = User.query.filter_by(id=id).first_or_404()
    elif tablename == 'Role':
        table = Role.query.filter_by(id=id).first_or_404()
    else:
        return jsonify({'status': 0})

    db.session.delete(table)
    db.session.commit()
    return jsonify({'status': 1})
