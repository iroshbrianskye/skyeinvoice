from datetime import datetime, timedelta
from hashlib import md5
from time import time
from flask import current_app, url_for
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from collections import OrderedDict
import jwt
import json
from app import db, login
import redis
import rq
import base64
import os

#roles
class Role(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    role = db.Column(db.String(64))
    access_quotation = db.Column(db.Boolean, default=False)
    access_invoice = db.Column(db.Boolean, default=False)
    access_bill = db.Column(db.Boolean, default=False)
    access_payment = db.Column(db.Boolean, default=False)
    access_client = db.Column(db.Boolean, default=False)
    access_supplier = db.Column(db.Boolean, default=False)
    access_user = db.Column(db.Boolean, default=False)
    permissions = db.relationship('Permission', backref='role', uselist=False, cascade='all,delete-orphan')
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user = db.relationship('User', backref='role', uselist=False, cascade='all,delete-orphan')

    def __repr__(self):
        return '<Role {}>'.format(self.id)

    def to_dict_role(self):
        data = {
            'id': self.id,
            'access_quotation': self.access_quotation,
            'access_invoice': self.access_invoice,
            'access_bill': self.access_bill,
            'access_payment': self.access_payment,
            'access_client': self.access_client,
            'access_supplier': self.access_supplier,
            'access_user': self.access_user,
            'read_quotation': self.permissions.read_quotation,
            'read_invoice': self.permissions.read_invoice,
            'read_bill': self.permissions.read_bill,
            'read_payment': self.permissions.read_payment,
            'read_client': self.permissions.read_client,
            'read_supplier': self.permissions.read_supplier,
            'read_user': self.permissions.read_user,
            'write_quotation': self.permissions.write_quotation,
            'write_invoice': self.permissions.write_invoice,
            'write_bill': self.permissions.write_bill,
            'write_payment': self.permissions.write_payment,
            'write_client': self.permissions.write_client,
            'write_supplier': self.permissions.write_supplier,
            'write_user': self.permissions.write_user,
            'create_quotation': self.permissions.create_quotation,
            'create_invoice': self.permissions.create_invoice,
            'create_bill': self.permissions.create_bill,
            'create_payment': self.permissions.create_payment,
            'create_client': self.permissions.create_client,
            'create_supplier': self.permissions.create_supplier,
            'create_user': self.permissions.create_user,
            'approve_quotation': self.permissions.approve_quotation,
            'approve_invoice': self.permissions.approve_invoice,
            'approve_bill': self.permissions.approve_bill,
            'approve_payment': self.permissions.approve_payment,
            'approve_client': self.permissions.approve_client,
            'approve_supplier': self.permissions.approve_supplier,
            'approve_user': self.permissions.approve_user,
            'delete_quotation': self.permissions.delete_quotation,
            'delete_invoice': self.permissions.delete_invoice,
            'delete_bill': self.permissions.delete_bill,
            'delete_payment': self.permissions.delete_payment,
            'delete_client': self.permissions.delete_client,
            'delete_supplier': self.permissions.delete_supplier,
            'delete_user': self.permissions.delete_user
        }
        return data

    def from_dict(self, data):
        for field in ['access_quotation','access_invoice','access_bill','access_payment','access_client','access_supplier','access_user']:
            if field in data:
                if data[field] == 'false':
                    setattr(self, field, False)
                elif data[field] == 'true':
                    setattr(self, field, True)

    def from_dict_first_registration(self):
        for field in ['access_quotation','access_invoice','access_bill','access_payment','access_client','access_supplier','access_user']:
            setattr(self, field, True)

#user login
class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    role_id=db.Column(db.Integer, db.ForeignKey('role.id'))
    phone_number = db.Column(db.String(120))
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)
    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
            digest, size)

    def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            current_app.config['SECRET_KEY'],
            algorithm='HS256').decode('utf-8')

    @staticmethod
    def verify_reset_password_token(token):
        try:
            id = jwt.decode(token, current_app.config['SECRET_KEY'],
                            algorithms=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)

    def get_token(self, expires_in=3600):
        now = datetime.utcnow()
        if self.token and self.token_expiration > now + timedelta(seconds=60):
            return self.token
        self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
        self.token_expiration = now + timedelta(seconds=expires_in)
        db.session.add(self)
        return self.token

    def revoke_token(self):
        self.token_expiration = datetime.utcnow() - timedelta(seconds=1)

    @staticmethod
    def check_token(token):
        user = User.query.filter_by(token=token).first()
        if user is None or user.token_expiration < datetime.utcnow():
            return None
        return user

    def invoice_created(self,quotation):
        return Invoice.query.filter_by(quotation=quotation).count()

    def to_dict(self, include_email=False):
        data = {
            'id': self.id,
            'username': self.username,
            'last_seen': self.last_seen.isoformat() + 'Z',
            'about_me': self.about_me,
            'post_count': self.posts.count(),
            'follower_count': self.followers.count(),
            'followed_count': self.followed.count(),
            '_links': {
                'self': url_for('api.get_user', id=self.id),
                'followers': url_for('api.get_followers', id=self.id),
                'followed': url_for('api.get_followed', id=self.id),
                'avatar': self.avatar(128)
            }
        }
        if include_email:
            data['email'] = self.email
        return data

    def from_dict(self, data, new_user=False):
        for field in ['username', 'email', 'about_me']:
            if field in data:
                setattr(self, field, data[field])
        if new_user and 'password' in data:
            self.set_password(data['password'])


@login.user_loader
def load_user(id):
    return User.query.get(int(id))

#clients
class Client(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    client_name = db.Column(db.String(64))
    client_type = db.Column(db.String(64))
    phone_number = db.Column(db.Integer, index=True)
    email = db.Column(db.String(64), index=True)
    location = db.Column(db.String(140))
    quotations = db.relationship('Quotation', backref='client', lazy='dynamic')
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)


    def __repr__(self):
        return '<Client {}>'.format(self.id)

#clients
class Supplier(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    supplier_name = db.Column(db.String(64))
    supplier_type = db.Column(db.String(64))
    phone_number = db.Column(db.Integer, index=True)
    email = db.Column(db.String(64), index=True)
    location = db.Column(db.String(140))
    bank_name       = db.Column(db.String(64))
    bank_country    = db.Column(db.String(64))
    bank_address    = db.Column(db.String(120))
    account_number  = db.Column(db.String(120))
    bills = db.relationship('Bill', backref='supplier', lazy='dynamic')
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)


    def __repr__(self):
        return '<Client {}>'.format(self.id)

#quotations
class Quotation(db.Model):
    STATUS = OrderedDict([('pending', 'Pending'),('rejected', 'Rejected'),('approved', 'Approved')])
    id = db.Column(db.Integer, primary_key=True)
    quotation_name = db.Column(db.String(64))
    amount = db.Column(db.Integer)
    description = db.Column(db.String(140))
    quotation_no = db.Column(db.String(140))
    client_id = db.Column(db.Integer, db.ForeignKey('client.id'))
    status = db.Column(db.Enum(*STATUS, name='status_types', native_enum=False), index=True, nullable=True, server_default='pending')
    quotation_date = db.Column(db.DateTime, index=True)
    invoice = db.relationship('Invoice', backref='quotation', uselist=False, cascade='all,delete-orphan')
    products = db.relationship('Product', backref='quotation', lazy='dynamic', cascade='all,delete-orphan')
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Quotation {}>'.format(self.id)



#invoice
class Invoice(db.Model):
    STATUS = OrderedDict([('pending', 'Pending'),('rejected', 'Rejected'),('approved', 'Approved')])
    id = db.Column(db.Integer, primary_key=True)
    invoice_no = db.Column(db.String(64))
    date_issued = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    date_due = db.Column(db.DateTime)
    status = db.Column(db.Enum(*STATUS, name='status_types', native_enum=False), index=True, nullable=True, server_default='pending')
    quotation_id=db.Column(db.Integer, db.ForeignKey('quotation.id'))
    payment = db.relationship('Payment', backref='invoice', lazy='dynamic', cascade='all,delete-orphan')
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Invoice {}>'.format(self.id)

#bills
class Bill(db.Model):
    STATUS  = OrderedDict([('pending', 'Pending'),('rejected', 'Rejected'),('approved', 'Approved')])
    id = db.Column(db.Integer, primary_key=True)
    bill_name = db.Column(db.String(64))
    bill_no = db.Column(db.String(64))
    amount = db.Column(db.Integer)
    description = db.Column(db.String(140))
    supplier_id = db.Column(db.Integer, db.ForeignKey('supplier.id'))
    status = db.Column(db.Enum(*STATUS, name='status_types', native_enum=False), index=True, nullable=True, server_default='pending')
    date_issued = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    date_due = db.Column(db.DateTime, index=True)
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Bill {}>'.format(self.id)

#payment
class Payment(db.Model):
    STATUS  = OrderedDict([('pending', 'Pending'),('rejected', 'Rejected'),('approved', 'Approved')])
    MODE  = OrderedDict([('cash', 'Cash'),('cheque', 'Cheque'),('online-payment', 'Online-Payment')])
    id = db.Column(db.Integer, primary_key=True)
    note = db.Column(db.String(140))
    amount = db.Column(db.Integer)
    invoice_id = db.Column(db.Integer, db.ForeignKey('invoice.id'))
    status = db.Column(db.Enum(*STATUS, name='status_types', native_enum=False), index=True, nullable=True, server_default='pending')
    payment_date = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    payment_mode =db.Column(db.Enum(*MODE, name='payment_mode', native_enum=False), index=True, nullable=True, server_default='cash')
    payment_type = db.Column(db.String(64))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Bill {}>'.format(self.id)


#product
class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    product = db.Column(db.String(64))
    size = db.Column(db.String(64))
    quantity = db.Column(db.Integer)
    price = db.Column(db.Integer)
    total = db.Column(db.Integer)
    quotation_id=db.Column(db.Integer, db.ForeignKey('quotation.id'))


    def __repr__(self):
        return '<Product {}>'.format(self.id)

#permissions
class Permission(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    read_quotation = db.Column(db.Boolean, default=False)
    read_invoice = db.Column(db.Boolean, default=False)
    read_bill = db.Column(db.Boolean, default=False)
    read_payment = db.Column(db.Boolean, default=False)
    read_client = db.Column(db.Boolean, default=False)
    read_supplier = db.Column(db.Boolean, default=False)
    read_user = db.Column(db.Boolean, default=False)
    write_quotation = db.Column(db.Boolean, default=False)
    write_invoice = db.Column(db.Boolean, default=False)
    write_bill = db.Column(db.Boolean, default=False)
    write_payment = db.Column(db.Boolean, default=False)
    write_client = db.Column(db.Boolean, default=False)
    write_supplier = db.Column(db.Boolean, default=False)
    write_user = db.Column(db.Boolean, default=False)
    create_quotation = db.Column(db.Boolean, default=False)
    create_invoice = db.Column(db.Boolean, default=False)
    create_bill = db.Column(db.Boolean, default=False)
    create_payment = db.Column(db.Boolean, default=False)
    create_client = db.Column(db.Boolean, default=False)
    create_supplier = db.Column(db.Boolean, default=False)
    create_user = db.Column(db.Boolean, default=False)
    approve_quotation = db.Column(db.Boolean, default=False)
    approve_invoice = db.Column(db.Boolean, default=False)
    approve_bill = db.Column(db.Boolean, default=False)
    approve_payment = db.Column(db.Boolean, default=False)
    approve_client = db.Column(db.Boolean, default=False)
    approve_supplier = db.Column(db.Boolean, default=False)
    approve_user = db.Column(db.Boolean, default=False)
    delete_quotation = db.Column(db.Boolean, default=False)
    delete_invoice = db.Column(db.Boolean, default=False)
    delete_bill = db.Column(db.Boolean, default=False)
    delete_payment = db.Column(db.Boolean, default=False)
    delete_client = db.Column(db.Boolean, default=False)
    delete_supplier = db.Column(db.Boolean, default=False)
    delete_user = db.Column(db.Boolean, default=False)
    role_id = db.Column(db.Integer, db.ForeignKey('role.id'))
    created_at = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Role {}>'.format(self.id)

    def from_dict_permissions(self, data):
        for field in ['read_quotation','read_invoice','read_bill','read_payment','read_client','read_supplier','read_user','write_quotation','write_invoice','write_bill','write_payment','write_client','write_supplier','write_user',
                      'create_quotation','create_invoice','create_bill','create_payment','create_client','create_supplier','create_user','approve_quotation','approve_invoice','approve_bill','approve_payment',
                      'approve_client','approve_supplier','approve_user','delete_quotation','delete_invoice','delete_bill','delete_payment','delete_client','delete_supplier','delete_user']:
            if field in data:
                if data[field] == 'false':
                    setattr(self, field, False)
                elif data[field] == 'true':
                    setattr(self, field, True)

    def from_dict_permissions_first_registration(self):
        for field in ['read_quotation','read_invoice','read_bill','read_payment','read_client','read_supplier','read_user','write_quotation','write_invoice','write_bill','write_payment','write_client','write_supplier','write_user',
                      'create_quotation','create_invoice','create_bill','create_payment','create_client','create_supplier','create_user','approve_quotation','approve_invoice','approve_bill','approve_payment',
                      'approve_client','approve_supplier','approve_user','delete_quotation','delete_invoice','delete_bill','delete_payment','delete_client','delete_supplier','delete_user']:
            setattr(self, field, True)

#company settings
class Company(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    company_name    = db.Column(db.String(64))
    contact_person  = db.Column(db.String(64))
    address         = db.Column(db.String(120))
    country         = db.Column(db.String(64))
    city            = db.Column(db.String(64))
    state           = db.Column(db.String(64))
    postal_code     = db.Column(db.String(64))
    email           = db.Column(db.String(64), index=True)
    phone_number    = db.Column(db.Integer, index=True)
    bank_name       = db.Column(db.String(64))
    bank_country    = db.Column(db.String(64))
    bank_address    = db.Column(db.String(120))
    account_number  = db.Column(db.String(120))

    def __repr__(self):
        return '<Company {}>'.format(self.id)

#theme settings
class Theme(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    company_name    = db.Column(db.String(64))
    logo            = db.Column(db.String(64))
    favicon         = db.Column(db.String(120))

    def __repr__(self):
        return '<Theme {}>'.format(self.id)
