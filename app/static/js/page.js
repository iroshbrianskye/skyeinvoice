$(function() {
    $("div[data-toggle=fieldset]").each(function() {
        var $this = $(this);

        //Add new entry
        $this.find("button[data-toggle=fieldset-add-row]").click(function() {

            var target = $($(this).data("target"))
            //console.log(target);
            var oldrow = target.find("[data-toggle=fieldset-entry]:last");
            var row = oldrow.clone(true, true);
            //console.log(row.find(":input")[0]);
            var elem_id = row.find(":input")[0].id;
            var elem_num = parseInt(elem_id.replace(/.*-(\d{1,4})-.*/m, '$1')) + 1;
            row.attr('data-id', elem_num);

            row.find(":input").each(function() {
                //console.log(this);
                var id = $(this).attr('id').replace('-' + (elem_num - 1) + '-', '-' + (elem_num) + '-');
                $(this).attr('name', id).attr('id', id).val('').removeAttr("checked");
            });
            oldrow.after(row);
        }); //End add new entry

        //Remove row
        $this.find("button[data-toggle=fieldset-remove-row]").click(function() {
            if($this.find("[data-toggle=fieldset-entry]").length > 1) {
                var thisRow = $(this).closest("[data-toggle=fieldset-entry]");
                thisRow.remove();
            }
            var oldrow = $("#products-table").find("[data-toggle=fieldset-entry]:last");
            var row = oldrow.clone(true, true);
            var elem_id = row.find(":input")[0].id;
            var elem_num = parseInt(elem_id.replace(/.*-(\d{1,4})-.*/m, '$1')+1);
            var total_amount=0
             for(var x=0;x < elem_num;x++)
             {

               var price = $("input[name=products-"+[x]+"-price]").val();
               var quantity= $("input[name=products-"+[x]+"-quantity]").val();

               if(price != ''  && quantity != '' && isNaN(price) == false && isNaN(quantity) == false  )
               {

                 total=price * quantity;
                 total_amount =total + total_amount
               }
             }
                 $("input[name=amount]").val(total_amount)
        }); //End remove row
    });
});
