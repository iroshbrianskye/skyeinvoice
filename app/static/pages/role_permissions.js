
$(document).ready(function() {
//first get role_permissions_values
$.ajax({
	type 		  : 'GET',
	url 		  : '/skyeinvoice/role_permissions_values',
	dataType 	: 'json',
	encode 		: true
}).done(function(response) {
	if (response.status == 1){
		var responsedata=response.data;
		for (var data in responsedata) {
			if (responsedata.hasOwnProperty(data)) {
					$('input[name='+data+']').prop('checked', responsedata[data])
				}
			}
			$('input[name="role-id"]').val(responsedata.id)
			Session.set('role',responsedata)
		}
	}).fail(function(responsedata) {
			alertify.error('Error: Could not contact server.');
});
//create or add role
$('.create_role').submit(function(event) {
		 var formData = {
					 'role'              : $('input[name=role]').val(),
			 };

		 $.ajax({
			 type 		: 'POST',
			 url 		: '/skyeinvoice/add_role',
			 data 		: formData,
			 dataType 	: 'json',
			 encode 		: true
		 }).done(function(data) {
			 if(data.status == 1){
				 alertify.success('role added successfully')
				 window.location.replace('/skyeinvoice/role_permissions');
			 }else
			 {
				 alertify.error('Error: Role not added')
			 }

			 }).fail(function(data) {
				 alertify.error('Error: Could not contact server.')
			 });
	event.preventDefault();
});

//edit roles
$('.edit_role').submit(function(event) {
			var formData = {
						'role'              : $('input[name=edit-role]').val(),
				};
			var id = $('input[name=id]').val();
			$.ajax({
				type 		: 'POST',
				url 		: '/skyeinvoice/edit_role/'+id,
				data 		: formData,
				dataType 	: 'json',
				encode 		: true
			}).done(function(data) {
				if(data.status == 1){
					alertify.success('role edited successfully')
					window.location.replace('/skyeinvoice/role_permissions');

				}else
				{
					alertify.error('Error: Role not added')
				}

				}).fail(function(data) {
					alertify.error('Error: Could not contact server.')
				});
	event.preventDefault();
  });

//update Module
$('.create_module').submit(function(event) {
	    var responsedata = Session.get("role")
			var formData ={}
	for (var data in responsedata) {
			if (responsedata.hasOwnProperty(data)) {
					formData[data] = $('input[name='+data+']').is(":checked")
				}
			}
			formData['id'] = $('input[name=role-id]').val();
			$.ajax({
				type 		: 'POST',
				url 		: '/skyeinvoice/edit_role_permissions_js',
				data 		: formData,
				dataType 	: 'json',
				encode 		: true
			}).done(function(data) {
				if(data.status == 1){
					alertify.success('module access edited successfully')

				}else
				{
					alertify.error('Error: module access not added')
				}

				}).fail(function(data) {
					alertify.error('Error: Could not contact server.')
				});
	event.preventDefault();
  });

});

//get edit role data
function edit_role(id) {
  $.ajax({
          type 		: 'GET',
          url 		: '/skyeinvoice/edit_role/'+id,
          dataType 	: 'json',
          encode 		: true
        }).done(function(response) {
            if(response['status'] == 1)
                  $('input[name="edit-role"]').val(response.data.role)
                  $('input[name="id"]').val(response.data.id)
  }).fail(function() {
    alertify.error('Error: Could not contact server.');
  });

}
//
//retrieve module access from //
function role(id,destElem) {
	$(".roles-menu>ul>li.active").removeClass("active");
	$(destElem).addClass("active");
	var formData = {'id': id};
		$.ajax({
			type 		  : 'POST',
			url 		  : '/skyeinvoice/role_permissions_values',
			data 			: formData,
			dataType 	: 'json',
			encode 		: true
		}).done(function(response) {
			if (response.status == 1){
				var responsedata=response.data;
				for (var data in responsedata) {
					if (responsedata.hasOwnProperty(data)) {
							$('input[name='+data+']').prop('checked', responsedata[data])
						}
					}
					$('input[name="role-id"]').val(responsedata.id)
					Session.set('role',responsedata)
				}
		}).fail(function(responsedata) {
				alertify.error('Error: Could not contact server.');
	});
}
//re
function delete_role(id, table) {
				alertify.confirm( 'Confirm deletion', 'Are you sure you want to delete '+table+'?', function() {
					$.post('/skyeinvoice/delete', {
							id: id,
							table:table
					}).done(function(response) {
						if(response['status'] == 1){
							alertify.success(table+' deleted successfully');
							window.location.replace('/skyeinvoice/role_permissions')
						}
						else {
							alertify.error('Error: wrong table name')
						}
					}).fail(function() {
							alertify.error('Error: Could not contact server.')
					});
				}
						 , function() { alertify.error('Cancelled') });

		}
